var HomeViewController = FPObject.extend({

    init: function() {
        this._super();
    },

    showInWindow: function(wnd) {
        var self = this;
        var uiLoader = new FPUILoader();
            uiLoader.loadXML({
                files : ["home.xml"],
                success: function(views) {
                    self.mainView = views["home.xml"];
                    wnd.setLayout(self.mainView);
                     

                    var gridView = self.mainView.getViewByName("grid_view_list");
                    var menuDataModel = new FPDataModel();
                    menuDataModel.setData(
                            [{
                                  title: 'Locate a Distributor',
                                  image: 'menu_map',
                                  icon: 'ic_about',
                                  id: 0
                              }, 
                              {
                                  title: 'Product Selector',
                                  image: 'menu_product_selector',
                                  icon: 'ic_ps',
                                  id: 1
                              }, 
                              {
                                  title: 'Product Comparison',
                                  image: 'menu_product_comparison',
                                  icon: 'ic_pc',
                                  id: 2
                              }, 
                              {
                                  title: 'Products',
                                  image: 'menu_products',
                                  icon: 'ic_product',
                                  id: 3
                              }, 
                              {
                                  title: 'Technical Information',
                                  image: 'menu_technical_information',
                                  icon: 'ic_ti',
                                  id: 4
                              }, 
                              {
                                  title: 'Applications',
                                  image: 'menu_applications',
                                  icon: 'ic_application',
                                  id: 5
                              }
                              ]);
                    gridView.setDataModel(menuDataModel);
                    gridView.addEventListener({
                        eventName: "onItemClick",
                        callback: function(item) 
                        {    
                              switch (item.id) 
                              {
                                case 0: 
                                {
                                  var distributorWindow = new LocateDistributorVC();
                                  wnd.transitionToWindow(distributorWindow);
                                  break;
                                }
                            }
                        }
                    });


                }
            });
       
    }



                   
});