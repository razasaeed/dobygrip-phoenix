var DrawerMenuDataModel = FPDataModel.extend({
    init: function() {
        this._super();
        this.setData([
            {
                title: "Home",
                id: 0
            },
            {
                title: "About Us",
                id: 1
            }, 
            {
                title: "Locate A Distributor",
                id: 2
            }, 
            {
                title: "Register/Login",
                id: 3
            }, 
            {
                title: "Product Comparison",
                id: 4
            }, 
            {
                title: "Product Selector",
                id: 5
            }, 
            {
                title: "Technical Information",
                id: 6
            }, 
            {
                title: "Products",
                id: 7
            }, 
            {
                title: "View Cart",
                id: 8
            }, 
            {
                title: "Application",
                id: 9
            }, 
            {
                title: "Help",
                id: 10
            }, 
            {
                title: "Privacy",
                id: 11
            }, 
            {
                title: "Terms",
                id: 12
            }

        ]);
    }
});