var LocateDistributorVC = FPWindow.extend({
    init: function(item) {
        this._super();
        var self = this;
        this.setTitle("Locate a Distributor");
        var uiLoader = new FPUILoader();
        uiLoader.loadXML({
            files : ["distributor.xml"],
            success: function(views) {
                self.setTheme({
                    primaryColor: "#013A6A",
                    accentColor: "#013A6A"
                });

                var recipeLayout = views["distributor.xml"];
                self.setLayout(recipeLayout);
               

            },
            failure: function(error) {
                alert(error);
            }
        });
    }
});