var TechnicalViewController = FPObject.extend({

    init: function() {
        this._super();
    },

    showInWindow: function(wnd) {
        var self = this;
        var uiLoader = new FPUILoader();
            uiLoader.loadXML({
                files : ["technical.xml"],
                success: function(views) {
                    self.mainView = views["technical.xml"];
                    wnd.setLayout(self.mainView);
                     

                     var webView = self.mainView.getViewByName("wvTechnical");
                        webView.loadUrl({
                        url: "https://phoenixsdk.com/",
                        onLoad: function() {
                            // success
                            },
                        onFail: function(error) {
                        // report error
                        }
                        });

                

                }
            });
    }               
});