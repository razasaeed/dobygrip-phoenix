var MainWindow = FPDrawerWindow.extend({
    init: function() {
        this._super();
        var self = this;
        var uiLoader = new FPUILoader();
        
        uiLoader.loadXML({
            files : ["home.xml", "drawer.xml"],
            success: function(views) {
                self.setTitle("Doby Grip");
                self.setTheme({
                    primaryColor: "#013A6A",
                    accentColor: "#013A6A"
                });

                var drawerView = views["drawer.xml"];
                self.setDrawerLayout(drawerView);

                self.homeController = new HomeViewController();
                self.homeController.showInWindow(self);
                
                // setup drawer view
                var menuListView = drawerView.getViewByName("drawer_list_view");
                menuListView.setDataModel(new DrawerMenuDataModel());
                menuListView.addEventListener({
                    eventName: "onItemClick",
                    callback: function(item) {
                        switch (item.id) {
                            // // home
                            case 0: {
                                //self.setTitle("Live Project");
                                // self.recipeController.showInWindow(self, dataModel);
                                if (!self.homeController) {
                                    self.homeController = new HomeViewController();
                                }
                                self.homeController.showInWindow(self);
                                break;
                            }
                            case 6: {
                                //self.setTitle("Live Project");
                                // self.recipeController.showInWindow(self, dataModel);
                                if (!self.technicalController) {
                                    self.technicalController = new TechnicalViewController();
                                }
                               self.technicalController.showInWindow(self);
                                break;
                            }
                            // // Products
                            // case 1: {
                            //     //self.setTitle("Products");
                            //     if(!self.recipeController)
                            //     {
                            //         self.recipeController = new RecipeListViewController();
                            //     }
                            //     self.recipeController.showInWindow(self, DataManager.getInstance().getDataModel());
                            //     break;
                            // }
                            // // categories
                            // case 2: {
                            //    // self.setTitle("Categories");
                            //     if (!self.categoriesController) {
                            //         self.categoriesController = new CategoriesViewController();
                            //     }
                            //     self.categoriesController.showInWindow(self);
                            //     break;
                            // }
                            // // favorites
                            // case 3: {
                            //     //self.setTitle("My Favorites");
                            //     if (!self.favoritesController) {
                            //         self.favoritesController = new RecipeListViewController();
                            //         self.favoritesDataModel = DataManager.getInstance().getFavoritesDataModel();
                            //     }
                            //     self.favoritesController.showInWindow(self, self.favoritesDataModel, "empty_favorite_view.xml");
                            //     break;
                            // }
                            // // Videos
                            // case 4: {
                            //      //self.setTitle("Watch Videos");
                            //     if (!self.videoController) {
                            //         self.videoController = new VideoViewController();
                            //     }
                            //     self.videoController.showInWindow(self);
                            //     break;
                            // }
                        }
                    }
                });
            },
            failure: function(error) {
                alert(error);
            }
        });
    }
});