var SplashWindow = FPWindow.extend({
    init: function(item) {
        this._super();
        var self = this;
        
        var uiLoader = new FPUILoader();
        uiLoader.loadXML({
            files : ["splash.xml"],
            success: function(views) {
                self.setTheme({
                    primaryColor: "#013A6A",
                    accentColor: "#013A6A"
                });

                self.setLayout(views["splash.xml"]);
                
                //Perform a task after 3 sec one time only
                setTimeout(
                        function()
                        {
                            
                            var mainWindow = new MainWindow();
                            mainWindow.presentAsRootWindow();
                            
                    },3000);


                    //Perform a task after 3 sec repetatively
                    // setInterval(
                    //     function()
                    //     {
                    //         alert("Hello");
                    // },3000);
               
            },
            failure: function(error) {
                alert(error);
            }
        });
    }
});