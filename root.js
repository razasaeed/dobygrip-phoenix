include([
	"ui.js",
	"SplashWindow.js",
	 "MainWindow.js",
	"HomeViewController.js",
	// "VideoViewController.js",
	// "DataModel.js",
	"DrawerMenuDataModel.js",
	"TechnicalViewController.js",
	"LocateDistributorVC.js"
	// "CategoriesDataModel.js",
	// "DataManager.js",
	// "RecipeWindow.js",
	// "RecipeListViewController.js",
	// "CategoriesViewController.js"
	],
	function() {
		
		var splashWindow = new SplashWindow();
		splashWindow.presentAsRootWindow();
});